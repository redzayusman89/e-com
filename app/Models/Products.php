<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Products extends Model
{
    //
    use SoftDeletes;
    protected $table = 'products';
    protected $primaryKey = 'product_id';

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'product_createdby');
    }

    public function scopeSearch($query, $searchKey)
    {
        return $query
            ->orWhere('product_name', 'LIKE', '%' . $searchKey . '%')
            ->orWhere('product_manufacture', 'LIKE', '%' . $searchKey . '%')
            ->orWhere('product_category', 'LIKE', '%'. $searchKey . '%');
    }


}
