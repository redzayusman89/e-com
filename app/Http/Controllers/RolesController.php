<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;

use App\Http\Requests;

class RolesController extends Controller
{

    public function index(Request $request) {
        $search = '';
        $paginationSize = \Config::get('constants.PAGINATION_SIZE');

        if($request->s) {
            $search = $request->s;
            $roles = Role::search($request->s)->paginate($paginationSize);
        } else {
            $roles = Role::paginate($paginationSize);
        }

        return view('role.index', ['roles' => $roles, 'search'=>$search]);
    }

    public function create()
    {
        $owner = new Role();
        $owner->name         = 'owner';
        $owner->display_name = 'Project Owner'; // optional
        $owner->description  = 'User is the owner of a given project'; // optional
        $owner->save();

        $admin = new Role();
        $admin->name         = 'admin';
        $admin->display_name = 'User Administrator'; // optional
        $admin->description  = 'User is allowed to manage and edit other users'; // optional
        $admin->save();
    }
}
