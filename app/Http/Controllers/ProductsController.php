<?php

namespace App\Http\Controllers;

use App\Models\Products;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Laracasts\Flash\Flash;

class ProductsController extends Controller
{

    public function welcome()
    {
        $products = Products::all();

        return view('welcome', ['products' => $products]);
    }

    public function index(Request $request)
    {

        $search = '';
        $paginationSize = \Config::get('constants.PAGINATION_SIZE');

        if ($request->s) {
            $search = $request->s;
            $products = Products::search($request->s)->paginate($paginationSize);
        } else $products = Products::paginate($paginationSize);

        return view('admin.products.index', ['products' => $products]);

    }

    public function add(Request $request)
    {
        return view('admin.products.add');
    }

    public function create(Request $request)
    {


        $products = new Products();
        $products->product_name = $request->product_name;
        $products->product_desc = $request->product_desc;
        $products->product_category = $request->product_category;
        $products->product_manufacture = $request->product_manufacture;
        $products->product_price = $request->product_price;
       //$products->product_createdby = Auth::user()->id;

        if ($request->hasFile('product_image')) {

            $file = $request->file('product_image');

            $fileName = $request->product_name . '.jpg';
            $filePath = 'fileproducts/';

            //make dir if not exist
            if (!is_dir($filePath)) {
                mkdir($filePath, 0755, true);
            }

            // dd($file);
            Storage::put($filePath . $fileName, file_get_contents($file->getRealPath()));
            $products->product_image = $fileName;

        }
        $products->save();

        Flash::success('Success');
        return redirect("/products");
    }

    public function edit($productId) {
        return view('admin.products.edit', ['products' => Products::findOrFail($productId)]);
    }

    public function update(Request $request, $countryId) {


        $products = Products::findOrFail($countryId);

        $products->product_name = $request->product_name;
        $products->product_desc = $request->product_desc;
        $products->product_category = $request->product_category;
        $products->product_manufacture = $request->product_manufacture;
        $products->product_price = $request->product_price;
        $products->save();


        Flash::success('update product Success');

        return redirect("/products");
    }

    public function delete(Request $request, $countryId) {
        //avoid deleting self

        $products = Products::findOrFail($countryId);
        $products->country_updateby = Auth::user()->id;
        $products->save();
        $products->delete();

        Flash::success('DELETE Product success');
        return redirect('/products');
    }

    private function validateRequest(Request $request) {
        return Validator::make($request->all(), [
            'country_desc' => 'required|max:50',
            'country_code' => 'required|max:50'
        ]);
    }
}
