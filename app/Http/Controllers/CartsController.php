<?php

namespace App\Http\Controllers;

use App\Models\Carts;
use App\Models\Products;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Laracasts\Flash\Flash;

class CartsController extends Controller
{


    public function create(Request $request)
    {
        //dd($request);
        $carts = new Carts();
        if(isset($request->carts_users_id)){
            $carts->carts_users_id = Auth::user()->id;
        }
        $carts->carts_product_id = $request->carts_product_id;
        $carts->carts_total = $request->carts_total;

        Flash::success('Success: Added to your shopping cart!');
        $carts->save();

        return redirect("/");
    }

    public function view()
    {
        $carts = Carts::all();

        return view('welcome', ['carts' => $carts]);
    }

}
