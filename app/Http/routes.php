<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//Constant
View::share('APP_NAME', Config::get('constants.APP_NAME'));
View::share('APP_NAME_SHORT', Config::get('constants.APP_NAME_SHORT'));
View::share('ENT_NAME', Config::get('constants.ENT_NAME'));
View::share('ENT_NAME_SHORT', Config::get('constants.ENT_NAME_SHORT'));
View::share('DATE_TIME', Config::get('constants.DATE_TIME'));
View::share('DATE_TIME_FORMAT', Config::get('constants.DATE_TIME_FORMAT'));
View::share('VERSION', Config::get('constants.VERSION'));
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {

//    Route::get('/', function () {
//        return view('welcome');
//    })->middleware('guest');

//    Route::get('/tasks', 'TaskController@index');
//    Route::post('/task', 'TaskController@store');
//    Route::delete('/task/{task}', 'TaskController@destroy');

    Route::auth();
    Route::get('/', 'ProductsController@welcome');
    Route::get('/admin', 'AdminController@index');
   // Route::post('/task', 'TaskController@store');
    //Route::delete('/task/{task}', 'TaskController@destroy');

    Route::auth();

    Route::get('/products', 'ProductsController@index');
    Route::get('/products/add', 'ProductsController@add');
    Route::post('/products/create', 'ProductsController@create');
    Route::get('/products/edit/{id}', 'ProductsController@edit');
    Route::post('/products/update/{id}', 'ProductsController@update');

    Route::post('/carts/create', 'CartsController@create');
    Route::get('/carts/view', 'CartsController@view');

    Route::get('storage/{image}', function ($image = null) {
        $path = storage_path() . '/app/fileproducts/' . $image;

        if (file_exists($path)) {
            return Response::download($path);
        }
    });

    //role
    Route::get('/role',
        [
            'uses' => 'RolesController@index',
            //'can' => 'view.app-setting-role'
        ]
    );
    Route::match(['get', 'post'], '/role/add',
        [
            'uses' => 'RolesController@add',
            //'can' => 'create.app-setting-role'
        ]
    );
    Route::match(['get', 'put'],'/role/edit/{roleId}',
        [
            'uses' => 'RolesController@edit',
            //'can' => 'update.app-setting-role'
        ]
    );
    Route::delete('/role/delete/{roleId}',
        [
            'uses' => 'RolesController@delete',
           // 'can' => 'delete.app-setting-role'
        ]
    );

});
