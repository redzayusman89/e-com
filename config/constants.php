<?php

return [
    "APP_NAME" => "E-com",
    "APP_NAME_SHORT" => "OnApp",
    "ENT_NAME" => "",
    "ENT_NAME_SHORT" => "E-com",
    "VERSION" => "a1",
    "PAGINATION_SIZE" => 10,
    "DATE_FORMAT" => "d/m/Y",
    "DATE_TIME_FORMAT" => "d/m/Y g:ia"
];