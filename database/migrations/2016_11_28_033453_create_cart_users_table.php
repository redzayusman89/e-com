<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Carts_user', function (Blueprint $table) {
            $table->increments('carts_id');
            $table->integer('carts_users_id')->nullable();
            $table->integer('carts_product_id')->nullable();
            $table->string('carts_total', 100)->nullable();
            $table->integer('carts_updateby');
            $table->integer('carts_createdby');
            $table->timestamps(); //createDate & updateDate
            $table->softDeletes(); //deletedDate

            //indexing
            $table->index('carts_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Carts_user');
    }
}
