<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Products', function (Blueprint $table) {
            $table->increments('product_id');
            $table->string('product_name',500);
            $table->string('product_image', 100);
            $table->string('product_category', 100);
            $table->string('product_price',100);
            $table->string('product_desc',100);
            $table->string('product_manufacture',100);
            $table->integer('product_updateby');
            $table->integer('product_createdby');
            $table->timestamps(); //createDate & updateDate
            $table->softDeletes(); //deletedDate

            //indexing
            $table->index('product_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Products');
    }
}
