<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dt = new DateTime;

        //admin
        DB::table('users')->insert([
            'fullname' => 'Administrator',
            'email' => 'admin@email.com',
            'password' => bcrypt('password'),
            'verified' => 1
        ]);

        DB::table('users')->insert([
            'fullname' => 'Administrator 2',
            'email' => 'admin2@email.com',
            'password' => bcrypt('password'),
            'verified' => 1
        ]);

        //applicant
        DB::table('users')->insert([
            'fullname' => 'Applicant',
            'email' => 'applicant@email.com',
            'password' => bcrypt('password'),
            'verified' => 1
        ]);
        //applicant 1
        DB::table('users')->insert([
            'fullname' => 'Applicant1',
            'email' => 'applicant1@email.com',
            //'user_ic' => '950501146132',
            'password' => bcrypt('password'),
            'verified' => 1
        ]);
        //applicant 2
        DB::table('users')->insert([
            'fullname' => 'Applicant2',
            'email' => 'applicant2@email.com',
            'password' => bcrypt('password'),
            'verified' => 1
        ]);
    }
}
