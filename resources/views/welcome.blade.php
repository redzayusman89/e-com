@extends('layouts.app')
<script>
    function addcart(id)
    {
        {{--$.ajax({--}}
            {{--headers: {--}}
                {{--'X-CSRF-Token': $('input[name="_token"]').val()--}}
            {{--},--}}
            {{--type: 'post',--}}
            {{--url: '/admin/entry-requirement/update/{{$id}}',--}}
            {{--data: {--}}
                {{--requirements: dataToSubmit,--}}
                {{--intakeProgramLevel: activeLevel--}}
            {{--}--}}
        {{--}).done(function () {--}}
            {{--swal({--}}
                {{--title: "Sucess",--}}
                {{--text: "Requirement saved",--}}
                {{--type: "success",--}}
                {{--showCancelButton: false--}}
            {{--}, function () {--}}
                {{--// Redirect the user--}}
                {{--window.location.href = "/admin/entry-requirement/view/{{$id}}";--}}
            {{--});--}}
        {{--}).fail(function () {--}}
            {{--return swal("Error", "Something has failed. Unable to save.", "error");--}}
        {{--});--}}

//        $.ajaxSetup(
//                {
//                    headers: {
//                        'X-CSRF-Token': $('input[name="_token"]').val()
//                    }
//                });
//
//        var formData = {name: "ravi", age: "31"}; //Array
//
//        $.ajax({
//            url: "/carts/create",
//            type: "POST",
//            data: "id" + id,
//            context: document.body,
//            success: function (data, textStatus, jqXHR) {
//
//            },
//            error: function (jqXHR, textStatus, errorThrown) {
//
//            }
//        });

    }
</script>
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <p class="lead">Shop Name</p>
            <div class="list-group">
                <a href="#" class="list-group-item">Category 1</a>
                <a href="#" class="list-group-item">Category 2</a>
                <a href="#" class="list-group-item">Category 3</a>
            </div>
        </div>

        <div class="col-md-9">

            <div class="row carousel-holder">

                <div class="col-md-12">
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="item active">
                                <img class="slide-image" src="http://placehold.it/800x300" alt="">
                            </div>
                            <div class="item">
                                <img class="slide-image" src="http://placehold.it/800x300" alt="">
                            </div>
                            <div class="item">
                                <img class="slide-image" src="http://placehold.it/800x300" alt="">
                            </div>
                        </div>
                        <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>
                    </div>
                </div>

            </div>

            <div class="row">


                @foreach($products as $product)
                <div class="col-sm-4 col-lg-4 col-md-4">
                    <div class="thumbnail">
                        <?php $path = storage_path() . '/app/fileproducts/'.$product->product_image;?>
                        <img src="{{$path}}" width="100" height="100" alt="">
                        <div class="caption">
                            <h4 class="pull-right">RM{{$product->product_price}}</h4>
                            <h4><a href="#">{{strtoupper($product->product_name)}}</a>
                            </h4>
                            <p>{{$product->product_desc}}.</p>
                        </div>
                            <form class="form-horizontal" action="/carts/create" method="post">
                                {!! csrf_field() !!}
                                <input type="hidden" name="carts_product_id" value="{{$product->product_id}}">
                                <div class="ratings">

                                    {{--<p class="pull-right">12 reviews</p>--}}
                                    <p>
                                        <button type="submit" class="btn btn-default">
                                            Add to Cart
                                        </button>

                                    </p>
                                </div>
                            </form>

                    </div>
                </div>
                @endforeach

                {{--<div class="col-sm-4 col-lg-4 col-md-4">--}}
                    {{--<div class="thumbnail">--}}
                        {{--<img src="http://placehold.it/320x150" alt="">--}}
                        {{--<div class="caption">--}}
                            {{--<h4 class="pull-right">$74.99</h4>--}}
                            {{--<h4><a href="#">Third Product</a>--}}
                            {{--</h4>--}}
                            {{--<p>This is a short description. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>--}}
                        {{--</div>--}}
                        {{--<div class="ratings">--}}
                            {{--<p class="pull-right">31 reviews</p>--}}
                            {{--<p>--}}
                                {{--<span class="glyphicon glyphicon-star"></span>--}}
                                {{--<span class="glyphicon glyphicon-star"></span>--}}
                                {{--<span class="glyphicon glyphicon-star"></span>--}}
                                {{--<span class="glyphicon glyphicon-star"></span>--}}
                                {{--<span class="glyphicon glyphicon-star-empty"></span>--}}
                            {{--</p>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}

                {{--<div class="col-sm-4 col-lg-4 col-md-4">--}}
                    {{--<div class="thumbnail">--}}
                        {{--<img src="http://placehold.it/320x150" alt="">--}}
                        {{--<div class="caption">--}}
                            {{--<h4 class="pull-right">$84.99</h4>--}}
                            {{--<h4><a href="#">Fourth Product</a>--}}
                            {{--</h4>--}}
                            {{--<p>This is a short description. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>--}}
                        {{--</div>--}}
                        {{--<div class="ratings">--}}
                            {{--<p class="pull-right">6 reviews</p>--}}
                            {{--<p>--}}
                                {{--<span class="glyphicon glyphicon-star"></span>--}}
                                {{--<span class="glyphicon glyphicon-star"></span>--}}
                                {{--<span class="glyphicon glyphicon-star"></span>--}}
                                {{--<span class="glyphicon glyphicon-star-empty"></span>--}}
                                {{--<span class="glyphicon glyphicon-star-empty"></span>--}}
                            {{--</p>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}

                {{--<div class="col-sm-4 col-lg-4 col-md-4">--}}
                    {{--<div class="thumbnail">--}}
                        {{--<img src="http://placehold.it/320x150" alt="">--}}
                        {{--<div class="caption">--}}
                            {{--<h4 class="pull-right">$94.99</h4>--}}
                            {{--<h4><a href="#">Fifth Product</a>--}}
                            {{--</h4>--}}
                            {{--<p>This is a short description. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>--}}
                        {{--</div>--}}
                        {{--<div class="ratings">--}}
                            {{--<p class="pull-right">18 reviews</p>--}}
                            {{--<p>--}}
                                {{--<span class="glyphicon glyphicon-star"></span>--}}
                                {{--<span class="glyphicon glyphicon-star"></span>--}}
                                {{--<span class="glyphicon glyphicon-star"></span>--}}
                                {{--<span class="glyphicon glyphicon-star"></span>--}}
                                {{--<span class="glyphicon glyphicon-star-empty"></span>--}}
                            {{--</p>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}

                {{--<div class="col-sm-4 col-lg-4 col-md-4">--}}
                    {{--<h4><a href="#">Like this template?</a>--}}
                    {{--</h4>--}}
                    {{--<p>If you like this template, then check out <a target="_blank" href="http://maxoffsky.com/code-blog/laravel-shop-tutorial-1-building-a-review-system/">this tutorial</a> on how to build a working review system for your online store!</p>--}}
                    {{--<a class="btn btn-primary" target="_blank" href="http://maxoffsky.com/code-blog/laravel-shop-tutorial-1-building-a-review-system/">View Tutorial</a>--}}
                {{--</div>--}}

            </div>

        </div>
    </div>
</div>
@endsection
