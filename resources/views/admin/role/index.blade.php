@extends('layouts.admin')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="/">{{trans('admin/breadcrumb.home')}}</a></li>
        <li><a href="/">{{trans('admin/breadcrumb.app_setting')}}</a></li>
        <li><a href="/admin/role">{{trans('admin/breadcrumb.roles')}}</a></li>
        @permission('create.app-setting-role')
        <div class="pull-right">
            <a class="btn btn-default btn-xs" href="/admin/role/add">
                <i class="glyphicon glyphicon-plus"></i>
                Add Role
            </a>
        </div>
        @endpermission
    </ol>
@endsection


@section('content')
        <table class="table table-hover table-condensed">
            <thead>
                <tr>
                    <th>Role</th>
                    <th>Slug</th>
                    <th>Description</th>
                    <th>Created date</th>
                    <th>Update date</th>
                    {{--@permission('update.app-setting-role | delete.app-setting-role')--}}
                    <th>Actions</th>
                    {{--@endpermission--}}
                </tr>
            </thead>
            <tbody>
                @foreach($roles as $role)
                <tr>
                    <td>{{$role->name}}</td>
                    <td>{{$role->slug}}</td>
                    <td>{{$role->description}}</td>
                    <td>{{date(config('constants.DATE_TIME_FORMAT'), strtotime($role->created_at))}}</td>
                    <td>{{date(config('constants.DATE_TIME_FORMAT'), strtotime($role->updated_at))}}</td>
                    {{--@permission('update.app-setting-role | delete.app-setting-role')--}}
                    <td>
                        {{--@if($role->id != 1)--}}
                        <form action="/admin/role/delete/{{$role->id}}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}

                            {{--@permission('update.app-setting-role')--}}
                            <a href="/admin/role/edit/{{$role->id}}" class="btn btn-default btn-xs">
                                <i class="glyphicon glyphicon-edit"></i>
                                Edit
                            </a>
                            {{--@endpermission--}}

                            {{--@permission('delete.app-setting-role')--}}
                            <button type="button" class="btn btn-default btn-xs" onclick="confirmDeleteModal($(this).parent());">
                                <i class="glyphicon glyphicon-trash"></i>
                                Delete
                            </button>
                            {{--@endpermission--}}
                        </form>
                        @endif
                    </td>
                    {{--@endpermission--}}
                </tr>
                @endforeach
            </tbody>
        </table>

        {{--{!! $roles->render() !!}--}}
@endsection

