@extends('layouts.admin')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="/">Home</a></li>
        <li><a href="/">{{trans('admin/breadcrumb.app_setting')}}</a></li>
        <li><a href="/admin/role">{{trans('admin/breadcrumb.roles')}}</a></li>
        <li><a href="#">Edit</a></li>
    </ol>
@endsection

@section('content')

        <form action="" method="POST" class="form-horizontal">
            {{ method_field('PUT') }}
            {{ csrf_field() }}

            <div class="form-group required">
                <label for="name" class="col-sm-2 control-label">Role name</label>
                <div class="col-sm-10">
                    <input type="text" name="name" class="form-control" required="required" value="{{$role->name}}">
                </div>
            </div>
            <div class="form-group required">
                <label for="slug" class="col-sm-2 control-label">Slug</label>
                <div class="col-sm-10">
                    <input type="text" name="slug" class="form-control" required="required" value="{{$role->slug}}">
                </div>
            </div>
            <div class="form-group required">
                <label for="slug" class="col-sm-2 control-label">Description</label>
                <div class="col-sm-10">
                    <textarea name="description" class="form-control" rows="3" required="required">{{$role->description}}</textarea>
                </div>
            </div>

            <table class="table table-bordered ">
                <thead>
                <tr>
                    <th>Permissions Name</th>
                    <th>Permissions Description</th>
                    <th>Access</th>
                </tr>
                </thead>
                <tbody>

                @foreach($permissions as $permissionName => $slugs)
                    <tr>
                        <td>{{ $permissionName }}</td>
                        <td>{{\Kodeine\Acl\Models\Eloquent\Permission::where('name', $permissionName)->first()->description}}</td>
                        <td>
                            <div class="btn-group btn-group-sm" data-toggle="buttons">
                                @foreach($slugs as $slugName => $slugStatus)
                                    <input type="hidden" name="permissions[{{$permissionName}}][{{$slugName}}]" id="h-{{$permissionName}}-{{$slugName}}" value="0" />
                                    <label class="btn btn-default {{ $slugStatus == true ? "active" : "" }}">
                                        <input type="checkbox"
                                               name="permissions[{{$permissionName}}][{{$slugName}}]"
                                               id="v-{{$permissionName}}-{{$slugName}}"
                                               autocomplete="off"
                                               value="1"
                                               {{ $slugStatus == true ? "checked" : "" }}
                                        > {{ $slugName }}
                                    </label>
                                @endforeach
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <div class="well">
                <div class="text-center">
                    <div class="form-group">

                        <button type="submit" class="btn btn-default">
                            <i class="glyphicon glyphicon-save"></i>
                            {{trans('admin/common.btn_save')}}
                        </button>
                        <a href="/admin/role" class="btn btn-default">
                            <i class="glyphicon glyphicon-remove"></i>
                            {{trans('admin/common.btn_cancel')}}
                        </a>

                    </div>
                </div>
            </div>
        </form>

@endsection