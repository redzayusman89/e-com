@extends('layouts.admin')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="/">Home</a></li>
        <li><a href="/admin/country">Users</a></li>
        <li><a href="#">View User</a></li>
    </ol>
@endsection

@section('content')
    <div class="container">
        <div class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-2 control-label">Country Description</label>
                <div class="col-sm-10">
                    <p class="form-control-static">{{$countries->country_desc}}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Email</label>
                <div class="col-sm-10">
                    <p class="form-control-static">{{$countries->country}}</p>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <form class="form-inline" action="/admin/user/delete/{{$countries['country_id']}}" method="POST">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}

                        <a href="/admin/user/edit/{{$countries['country_id']}}" class="btn btn-default">Edit</a>
                        <button class="btn btn-default">Delete</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection