@extends('layouts.admin')
@section('content')
    <h1 class="page-header">Products</h1>
    <div class="pull-right">
        <a class="btn btn-default btn-xs" href="/products/add">
            <i class="glyphicon glyphicon-plus"></i>
            Add Products
        </a>
    </div>
    <table class="table table-bordered table-hover" id="dataTables-example">
        <thead>
        <tr>
            <th>No</th>
            <th>Products Name</th>
            <th>Price</th>
            <th>Description</th>
            <th>Images</th>
            <th>Category</th>
            <th>Manufacture</th>
            <th></th>
        </tr>
        </thead>
            <tbody>
            <?php $i=1;?>
            @foreach($products as $product)
                <tr>
                    <td>{{$i}}</td>
                    <td>{{$product['product_name']}}</td>
                    <td>{{$product['product_price']}}</td>
                    <td>{{$product['product_desc']}}</td>
                    <td>
                        <?php $path = storage_path() . '/app/fileproducts/'.$product['product_image'];?>
                        <img src="{{$path}}" width="50px" height="50px" />
                        {{--<img src="{{\Illuminate\Support\Facades\Storage::url('app/fileproducts/'.$product['product_image'])}}"--}}
                             {{--width="50px" height="50px"/>--}}
                        {{--<img src="{{storage_path() . '/app/fileproducts/' .$product['product_image']}}"width="50px" height="50px"></td>--}}
                    <td>{{$product['product_category']}}</td>
                    <td>{{$product['product_manufacture']}}</td>
                    <td>

                        <form class="confirm-delete form-inline"
                              action="/products/delete/{{$product['product_id']}}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}

                            <a href="/products/edit/{{$product['product_id']}}" class="btn btn-default btn-xs">
                                <i class="glyphicon glyphicon-edit"></i>
                                Edit
                            </a>
                            <button type="button" class="btn btn-default btn-xs" onclick="confirmDeleteModal($(this).parent());">
                                <i class="glyphicon glyphicon-trash"></i>
                                Delete
                            </button>
                        </form>

                    </td>
                </tr>
                <?php $i++;?>
            @endforeach
            </tbody>
        </table>

        {{--{!! $countries->render() !!}--}}


@endsection