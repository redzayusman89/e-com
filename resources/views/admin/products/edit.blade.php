@extends('layouts.admin')



@section('content')
    <h1 class="page-header"></h1>
    <div class="container">
        <!-- Display Validation Errors -->
        @include('common.errors')


        <form action="/products/update/{{$products->product_id}}" method="POST" class="form-horizontal">
            {{ csrf_field() }}

            <div class="form-group">
                <label class="col-md-4 control-label">Product Name</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="product_name" value="{{$products->product_name}}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">Product Description</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="product_desc" value="{{$products->product_desc}}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">Product Category</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="product_category" value="{{$products->product_category}}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">Product Manufacture</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="product_manufacture" value="{{$products->product_manufacture}}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">Product Price</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="product_price" value="{{$products->product_price}}">
                </div>
            </div>

            <!-- Add Task Button -->
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-default">
                        <i class="glyphicon glyphicon-save"></i> Save
                    </button>
                    <a href="/products" class="btn btn-default">
                        <i class="glyphicon glyphicon-remove-circle"></i> Cancel
                    </a>
                </div>
            </div>
        </form>
    </div>
@endsection