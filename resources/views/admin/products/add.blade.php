@extends('layouts.admin')

@section('content')
    <h1 class="page-header"></h1>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Add Product</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="/products/create" enctype="multipart/form-data">
                           {!! csrf_field() !!}
                            <div class="form-group">
                                <label class="col-md-4 control-label">Product Name</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="product_name" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Product Description</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="product_desc">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Product Category</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="product_category">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Product Manufacture</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="product_manufacture">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Product Price</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="product_price">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Product Image</label>
                                <div class="col-md-6">
                                    <input name="product_image" class="input-sm" type="file" >
                                    <small>Upload Your Product Image</small>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-default">
                                        <i class="glyphicon glyphicon-save"></i>
                                        Save
                                    </button>
                                    <a href="/products" class="btn btn-default">
                                        <i class="glyphicon glyphicon-remove"></i>
                                       Cancel
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
